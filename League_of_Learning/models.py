from django.db import models
from django.urls import reverse

class Post(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=200)
    question = models.TextField()
    answer = models.TextField(default = "To be answered.")

    def __str__(self):
        return self.question
    
    def get_absolute_url(self):
        return reverse('post')



               
