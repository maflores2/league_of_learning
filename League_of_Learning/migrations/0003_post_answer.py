# Generated by Django 2.1.3 on 2018-12-03 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('League_of_Learning', '0002_auto_20181127_1942'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='answer',
            field=models.TextField(default='Awaiting answer'),
        ),
    ]
