from django.views.generic import ListView
from django.views.generic.edit import CreateView
from . models import Post

class BlogListView(ListView):
    model = Post
    template_name = 'index.html'

class AboutListView(ListView):
    model = Post
    template_name = 'about.html'

class BlogCreateView(CreateView):
    model = Post
    template_name = 'post.html'
    fields = 'name','email','question'

class AnswersListView(ListView):
    model = Post
    template_name = 'qna.html'
    fields = 'name','email','question'

