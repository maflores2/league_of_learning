from django.urls import path
from . import views

urlpatterns = [
    path('', views.BlogListView.as_view(), name='index'),
    path('about', views.AboutListView.as_view(), name='about'),
    path('post', views.BlogCreateView.as_view(), name='post'),
    path('qna', views.AnswersListView.as_view(), name='qna')
]